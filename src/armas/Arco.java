package armas;

public class Arco extends Arma {
	public Arco(String nombre, int daño, int alcance, int acierto) {
		super(nombre, daño, alcance, acierto);
	}

	@Override
    public HabilidadEspecial habilidadEspecial() {
        return new HabilidadEspecial("Corredor", 1);
    }
}
