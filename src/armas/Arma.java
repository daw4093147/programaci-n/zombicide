package armas;

public class Arma {
	
	private String nombre;
	private int daño;
	private int alcance;
	private int acierto;
	
	public Arma(String nombre, int daño, int alcance, int acierto) {
        setNombre(nombre);
        setDaño(daño);
        setAlcance(alcance);
        setAcierto(acierto);
    }
	/*Constructor vacío para hacer el arma por defecto daga*/
	public Arma() {
		setNombre("Daga");
		setDaño(1);
		setAlcance(1);
		setAcierto(4);
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public int getDaño() {
		return daño;
	}
	
	public void setDaño(int daño) {
		this.daño = daño;
	}
	
	public int getAlcance() {
		return alcance;
	}
	
	public void setAlcance(int alcance) {
		this.alcance = alcance;
	}
	
	public int getAcierto() {
		return acierto;
	}
	
	public void setAcierto(int acierto) {
		this.acierto = acierto;
	}
	
	public HabilidadEspecial habilidadEspecial() {
		return null;
    }
	
}