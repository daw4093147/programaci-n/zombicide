package armas;

public class Espada extends Arma {
	public Espada(String nombre, int daño, int alcance, int acierto) {
		super(nombre, daño, alcance, acierto);
	}

	@Override
    public HabilidadEspecial habilidadEspecial() {
		return new HabilidadEspecial("Random" ,2);
    }
}
