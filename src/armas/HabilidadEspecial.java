package armas;

public class HabilidadEspecial {
    private int cantMuertos;
    private String zombie;

    public HabilidadEspecial(String zombieTipo, int cantMuertos) {
        setCantMuertos(cantMuertos);
        setZombie(zombieTipo);
    }

    public int getCantMuertos() {
        return cantMuertos;
    }
    
    public void setCantMuertos(int cantMuertos) {
    	this.cantMuertos = cantMuertos;
    }

    public String getZombie() {
        return zombie;
    }
    
    public void setZombie(String zombieTipo) {
    	this.zombie = zombieTipo;
    }
}
