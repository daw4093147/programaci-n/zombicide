package armas;

public class Hacha extends Arma {
	public Hacha(String nombre, int daño, int alcance, int acierto) {
		super(nombre, daño, alcance, acierto);
	}

	@Override
    public HabilidadEspecial habilidadEspecial() {
		return new HabilidadEspecial("Gordo", 1);
    }
}
