package armas;

public class Hechizo extends Arma {
	public Hechizo(String nombre, int daño, int alcance, int acierto) {
		super(nombre, daño, alcance, acierto);
	}

	@Override
    public HabilidadEspecial habilidadEspecial() {
		return new HabilidadEspecial("Caminante", 2);
    }
}

