package humanoides;

public class Humanoide {

	public String nombre;
	public int salud;
	public int saludMaxima;
	public boolean estaVivo;
	
	public Humanoide(String nombre, int saludMaxima, int salud, boolean estaVivo) {
        setNombre(nombre);
        setSaludMaxima(saludMaxima);
        setSalud(salud);
        setVivo(estaVivo);
	}
	
    public String getNombre() {
    	return this.nombre;
    }
    
    public void setNombre(String nombre) {
    	this.nombre = nombre;
    }
    
    public int getSalud() {
    	return this.salud;
    }
    
    public void setSalud(int salud) {
    	this.salud = salud;
    }
    
    public int getSaludMaxima() {
    	return this.saludMaxima;
    }
    
    public void setSaludMaxima(int saludMaxima) {
    	this.saludMaxima = saludMaxima;
    }
    
    public boolean getVivo() {
    	return this.estaVivo;
    }
    
    public void setVivo(boolean estaVivo) {
    	this.estaVivo = estaVivo;
    }


}
