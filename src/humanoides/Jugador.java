package humanoides;

import armas.Arma;

public class Jugador extends Humanoide {
	
	private Arma arma;
	private boolean pasaRonda;
	private boolean habilidadEspecialUsada;
	
	public Jugador(String nombre, int saludMaxima, int salud, boolean estaVivo, Arma arma) {
		super(nombre, saludMaxima, salud, estaVivo);
		setArma(arma);
		setpasaRonda(true);
	}
	
	 public Arma getArma() {
	        return arma;
	    }

	    public void setArma(Arma arma) {
	        this.arma = arma;
	    }

	    public boolean getpasaRonda() {
	        return pasaRonda;
	    }

	    public void setpasaRonda(boolean pasaRonda) {
	        this.pasaRonda = pasaRonda;
	    }
	    
	    public boolean getHabilidadEspecialUsada() {
	        return habilidadEspecialUsada;
	    }

	    public void setHabilidadEspecialUsada(boolean habilidadEspecialUsada) {
	        this.habilidadEspecialUsada = habilidadEspecialUsada;
	    
	    }
}