package humanoides;

public class Zombie extends Humanoide {
	 private int movimiento;
	 private int daño;
	 private String tipo;

	    public Zombie(String nombre, int saludMaxima, int salud, boolean estaVivo, int movimiento, int daño, String tipo) {
	        super(nombre, saludMaxima, salud, estaVivo);
	        setMovimiento(movimiento);
	        setDaño(daño);
	        setTipo(tipo);
	    }

	    public int getMovimiento() {
	        return movimiento;
	    }
	    
	    public void setMovimiento(int movimiento) {
	    	this.movimiento = movimiento;
	    }

	    public int getDaño() {
	        return daño;
	    }
	    
	    public void setDaño(int daño) {
	    	this.daño = daño;
	    }

	    public String getTipo() {
	        return tipo;
	    }
	    
	    public void setTipo(String tipo) {
	    	this.tipo = tipo;
	    }

}
