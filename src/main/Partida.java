package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;

import armas.Arma;
import armas.HabilidadEspecial;
import humanoides.Caminante;
import humanoides.Corredor;
import humanoides.Gordo;
import humanoides.Jugador;
import humanoides.Zombie;

public class Partida {
	// Se añaden colores para decorar algunos textos
	public static final String RESET = "\u001B[0m";
	public static final String ROJO = "\u001B[31m";
	public static final String CYAN = "\u001B[36m";
	
	private ArrayList<Jugador> jugadores;
	private ArrayList<Arma> armasDisponibles;
	private ArrayList<Zombie> zombies;
	private ArrayList<Arma> armasIniciales;
	private int nivel;
	private boolean finDelJuego;
	private int pocion;
	private int contNivel;

	// Constructor de la clase
	public Partida(ArrayList<Jugador> jugadores, ArrayList<Arma> armasDisponibles, int nivel, boolean finDelJuego, ArrayList<Zombie> zombies, ArrayList<Arma> armasIniciales) {
	    setJugador(jugadores);
	    setArma(armasDisponibles);
	    setNivel(nivel);
	    setfinDelJuego(finDelJuego);
	    setZombie(zombies);
	    setPociones(getNivel()*2);
	    setArmaInicial(armasIniciales);    
	    setContNivel(0);
	}
	
	// Getters y setters
	public ArrayList<Jugador> getJugador() {
		return jugadores;
	}
	
	public void setJugador(ArrayList<Jugador> jugadores) {
		this.jugadores = jugadores;
	}
	
	public ArrayList<Arma> getArma() {
		return armasDisponibles;
	}
	
	public void setArma(ArrayList<Arma> armasDisponibles) {
		this.armasDisponibles = armasDisponibles;
	}
	
	public ArrayList<Arma> getArmaInicial() {
		return armasIniciales;
	}
	
	public void setArmaInicial(ArrayList<Arma> armasIniciales) {
		this.armasIniciales = armasIniciales;
	}
	
	public int getNivel() {
		return nivel;
	}
	
	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	
	public boolean getfinDelJuego() {
		return finDelJuego;
	}
	
	public void setfinDelJuego(boolean finDelJuego) {
		this.finDelJuego = false;
	}
	
	public ArrayList<Zombie> getZombie() {
		return zombies;
	}
	
	public void setZombie(ArrayList<Zombie> zombies) {
		this.zombies = zombies;
	}
	
	public int getPociones() {
		return pocion;
	}
	
	public void setPociones(int pocion) {
		this.pocion = pocion;
	}
	
	public int getContNivel() {
		return contNivel;
	}
	
	public void setContNivel(int contNivel) {
		this.contNivel = contNivel;
	}
	// Inicia la partida y se eligen las opciones para cada jugador	    
	public void jugarPartida() {
	    Scanner sc = new Scanner(System.in);
		int ronda = 0;
		
	    do {
	        ronda++;
	        if (getZombie().isEmpty()) { // Si ya no quedan zombies se llama a la funcion subirNivel() y las rondas vuelven a 1
	        	subirNivel();
	        	ronda = 1;
			}
	        
	        for (int i = 0; i < getJugador().size(); i++) {
	        	Jugador jugadorActual = getJugador().get(i);
	        	
	        	if (jugadorActual.getVivo() == false) { 
	        		jugadorActual.setpasaRonda(false);
	                continue; 
	            }
	        	
	            menuJugador(ronda, jugadorActual);
	            int opcion = sc.nextInt();
	            sc.nextLine();  

	            switch (opcion) {
	                case 0:
	                    break;
	                case 1:
	                	atacar(jugadorActual);
	                    break;
	                case 2:
	                	habilidadEspecial(jugadorActual);
	                    break;
	                case 3:
	                	buscar(jugadorActual);
	                    break;
	                case 4:
	                	cambiarArma(jugadorActual);
	                    break;
	                case 5:
	                	usarPocion(jugadorActual);
	                	break;
	                default:
	                    System.out.println("Opción no válida.");
	            }
	         // Si no quedan más zombies se empieza se aumenta el nivel
	    	    if (getZombie().isEmpty()) {
	    	        jugarPartida();
	    	    }
	        }
	        ataqueZombies();
	    } while (!getfinDelJuego());
	}
	
	
	// Menú para cada jugador
	private void menuJugador(int ronda, Jugador jugador) {
			System.out.println("\n|----- NIVEL: " + getNivel() + " - RONDA: " + ronda + " ----|");
			System.out.print("\n==| ");
			for (Zombie zombie: getZombie()) {
		            System.out.print(zombie.getTipo() + " ");
		    }
			System.out.print("|==");
			System.out.println("\nJUGADOR:" + jugador.getNombre() + " S:" + jugador.getSalud() + " Arma[" +jugador.getArma().getNombre().toUpperCase() + " DAÑO: " + jugador.getArma().getDaño() + " ALC: " + jugador.getArma().getAlcance() + " ACIER: " + jugador.getArma().getAcierto() + "]" );
			System.out.println("1- Atacar");
			System.out.println("2- Habilidad especial");
			System.out.println("3- Buscar");
			System.out.println("4- Cambiar Arma");
			System.out.println("5- Curar");
			System.out.println("0- Pasar");
		
	}
	// Los zombies atacan a los jugadores al principio de cada ronda
	public void ataqueZombies() {
		System.out.println(ROJO + "\nAtacan los zombies" + RESET);
		for (Zombie zombie : getZombie()) {
	        int movimientoZombie = zombie.getMovimiento();

	        for (int i = 0; i < movimientoZombie; i++) {
	            int jugadorIndex = randJugador(getJugador());

	            if (jugadorIndex != -1) {
	                Jugador jugadorAtacado = getJugador().get(jugadorIndex);
	                if (jugadorAtacado.getpasaRonda()) {
		                int dañoZombie = zombie.getDaño();	                
		                if (dañoZombie >= jugadorAtacado.getSalud()) {
		                    System.out.println(ROJO + zombie.getNombre() + " ha atacado a " + jugadorAtacado.getNombre() + " y ha muerto" + RESET); 
		                    jugadorAtacado.setVivo(false);
		                    getJugador().remove(jugadorAtacado);
		                } else {
		                    jugadorAtacado.setSalud(jugadorAtacado.getSalud() - dañoZombie);
		                    System.out.println(ROJO + zombie.getNombre() + " ha atacado a " + jugadorAtacado.getNombre() + " y le ha causado " + dañoZombie + " de daño. Salud restante: " + jugadorAtacado.getSalud() + RESET);
		                }
	                }
	            }
	        }
	    }
		// Si ya no quedan jugadores vivos se acaba la partida
	    if (getJugador().isEmpty()) {
	        System.out.println(ROJO + "Todos los personajes han muerto. Has perdido la partida." + RESET);
	        System.exit(0);
	    }
	}
	// Se usa una poción para que el jugador pueda curarse
	public void usarPocion(Jugador jugador) {
	    Scanner sc = new Scanner(System.in);
	    if (getPociones() > 0) { // Verificar si hay pociones disponibles
	        System.out.println(getPociones() + " pociones disponibles.");
	        System.out.println("Jugadores disponibles para curar:"); // Salen los personajes con su vida
	        for (int i = 0; i < getJugador().size(); i++) {
	            Jugador jugadorActual = getJugador().get(i);
	            System.out.println((i + 1) + ". " + jugadorActual.getNombre() + " (Salud: " + jugadorActual.getSalud() + "/" + jugadorActual.getSaludMaxima() + ")");
	        }
	        int opcion = sc.nextInt(); // Se elige a qué personaje curar
	        if (opcion >= 1 && opcion <= getJugador().size()) {
	            Jugador jugadorSeleccionado = getJugador().get(opcion - 1);
	            setPociones(getPociones() - 1);

	            if (jugadorSeleccionado.getSalud() + 5 > jugadorSeleccionado.getSaludMaxima()) {
	                jugadorSeleccionado.setSalud(jugadorSeleccionado.getSaludMaxima());
	            } else {
	                jugadorSeleccionado.setSalud(jugadorSeleccionado.getSalud() + 5);
	            }

	            System.out.println(CYAN + "Has curado a " + jugadorSeleccionado.getNombre() + ". Salud actual: " + jugadorSeleccionado.getSalud() + RESET);
	        } else {
	            System.out.println("Opción no válida");
	        }
	    } else {
	        System.out.println("No te quedan más pociones");
	    }
	}
	// Atacar a un zombie o varios
	public void atacar(Jugador jugador) {
		int zombieIndex = randZombie(getZombie());

	    if (zombieIndex != -1) {
	        int daño = jugador.getArma().getDaño();
	        int alcance = jugador.getArma().getAlcance();

	        List<Integer> zombiesEliminados = new ArrayList<>();

	        for (int i = 0; i < alcance; i++) {
	            int currentZombieIndex = zombieIndex + i;
	            // Verificar si el índice actual está dentro del rango de zombies
	            if (currentZombieIndex < getZombie().size()) {
	                Zombie zombieAfectado = getZombie().get(currentZombieIndex);
	                // Verificar si el daño del arma supera la salud del zombie
	                if (daño > zombieAfectado.getSalud()) {
	                    Random rd = new Random();
	                    int randChance = rd.nextInt(7);	// Probabilidad de acierto
	                    // Verificar si el ataque tiene éxito según la probabilidad de acierto
	                    if (randChance > jugador.getArma().getAcierto()) {
	                        System.out.println(zombieAfectado.getNombre() + " ha muerto");
	                        zombiesEliminados.add(currentZombieIndex); // Se añaden los zombies eliminados a una lista
	                        habilidadEspecialZombies(zombieAfectado); // Activar habilidad especial de un zombie
	                    } else {
	                        System.out.println("Has fallado tu ataque contra " + zombieAfectado.getNombre() + " con un " + randChance);
	                    }
	                } else {
	                    System.out.println(zombieAfectado.getNombre() + " ha esquivado tu ataque");
	                }
	            }
	        }

	        // Eliminar los zombies después de la iteración
	        for (int i = zombiesEliminados.size() - 1; i >= 0; i--) {
	            getZombie().remove((int) zombiesEliminados.get(i));
	        }
	    }
	}
	//
	public void subirNivel() { // Se aumenta el nivel, se generan nuevos zombies, la habilidad especial puede volver a usarse y se dan pociones
		setNivel(getNivel()+1);
        initZombies(getZombie(), getNivel());
        for (Jugador jugador : getJugador()) {
            jugador.setHabilidadEspecialUsada(false);
        }
        setContNivel(getContNivel()+1);
        aumentarSalud();
        setPociones(getNivel()*2);
        System.out.println(CYAN + "Aumenta el nivel, has obtenido " + getPociones() + " pociones para esta ronda." + RESET);
	}
	// Se activa la habilidad especial y solo puede usarse 1 vez por nivel
	public void habilidadEspecial(Jugador jugador) {
		HabilidadEspecial ataque = jugador.getArma().habilidadEspecial();
	    if (jugador.getHabilidadEspecialUsada() || ataque == null) { // Si la habilidad especial ya sido utilizada, o no existe, se muestra un mensaje
	        if (ataque == null) {
	            System.out.println("No hay habilidad especial");
	        } else {
	            System.out.println("No puedes utilizar la habilidad especial hasta el siguiente nivel.");
	        }
	        return;          
	    }

	    // Se activa la habilidad especial dependiendo del tipo de arma
	    switch (ataque.getZombie()) {
	        case "Random":
	            System.out.println(CYAN + "La habilidad especial se ha activado! 2 zombies han sido eliminados." + RESET);
	            for (int i = 0; i < ataque.getCantMuertos(); i++) {
	                int randZombie = randZombie(getZombie());
	                if (randZombie != -1) {
	                    getZombie().remove(randZombie);
	                }
	            }
	            break;
	        case "Gordo":
	            System.out.println(CYAN + "La habilidad especial se ha activado! 1 Gordo ha sido eliminado." + RESET);
	            for (int i = 0; i < ataque.getCantMuertos(); i++) {
	                int toRemove = -1;
	                for (int j = 0; j < getZombie().size(); j++) {
	                    if (getZombie().get(j) instanceof Gordo) {
	                        toRemove = j;
	                    }
	                }
	                if (toRemove != -1) getZombie().remove(toRemove);
	            }
	            break;
	        case "Caminante":
	            System.out.println(CYAN + "La habilidad especial se ha activado! 2 caminantes han sido eliminados." + RESET);
	            for (int i = 0; i < ataque.getCantMuertos(); i++) {
	                int toRemove = -1;
	                for (int j = 0; j < getZombie().size(); j++) {
	                    if (getZombie().get(j) instanceof Caminante) {
	                        toRemove = j;
	                    }
	                }
	                if (toRemove != -1) getZombie().remove(toRemove);
	            }
	            break;
	        case "Corredor":
	            System.out.println(CYAN + "La habilidad especial se ha activado! 1 corredor ha sido eliminado." + RESET);
	            for (int i = 0; i < ataque.getCantMuertos(); i++) {
	                int toRemove = -1;
	                for (int j = 0; j < getZombie().size(); j++) {
	                    if (getZombie().get(j) instanceof Corredor) {
	                        toRemove = j;
	                    }
	                }
	                if (toRemove != -1) getZombie().remove(toRemove);
	            }
	            break;
	    }
	    jugador.setHabilidadEspecialUsada(true);
	}
	
	// Se activa la habilidad especial de los zombies 
	public void habilidadEspecialZombies(Zombie zombie) {
		Random rd = new Random();
		int activarHabEsp = rd.nextInt(101);
		// Evalúa el tipo de zombie y activa la habilidad especial correspondiente
		switch (zombie.getTipo()) {
        case "Caminante":
            if (activarHabEsp >= 95) {
                System.out.println(ROJO + "La habilidad especial del Caminante ha sido activada al morir!");
                System.out.println("La cantidad de Caminantes se ha doblado." + RESET);
                // Calcula la cantidad de Caminantes vivos y agrega nuevos Caminantes
                int caminantesVivos = (int) getZombie().stream().filter(z -> z instanceof Caminante).count();
                for (int i = 0; i < caminantesVivos; i++) {
                    getZombie().add(new Caminante("Caminante", 1, 1, true, 1, 1, "Caminante"));
                }
            }
            break;
        case "Gordo":
            if (activarHabEsp >= 95) {
                System.out.println(ROJO + "La habilidad especial del Gordo ha sido activada al morir!" + RESET);    
             // Filtra los Gordo vivos y elimina aleatoriamente uno si hay al menos dos
                List<Gordo> gordosVivos = getZombie().stream()
                        .filter(z -> z instanceof Gordo)
                        .map(z -> (Gordo) z)
                        .collect(Collectors.toList());
                if (gordosVivos.size() >= 2) {
                	Gordo gordoEliminado = gordosVivos.get(rd.nextInt(gordosVivos.size()));
                	getZombie().remove(gordoEliminado);
                	System.out.println(ROJO + "Se ha muerto otro gordo." + RESET);
                } 
            }
            break;
        case "Corredor":
        	if (activarHabEsp >= 95) {
                System.out.println(ROJO + "La habilidad especial del Corredor ha sido activada al morir!");
             // Busca y elimina todos los Corredores vivos
                List<Integer> indicesAEliminar = new ArrayList<>();

                for (int i = 0; i < getZombie().size(); i++) {
                    Zombie zombieActual = getZombie().get(i);
                    if (zombieActual instanceof Corredor) {
                        indicesAEliminar.add(i);
                    }
                }
             // Si hay Corredores vivos, los elimina y muestra un mensaje
                if (!indicesAEliminar.isEmpty()) {
                    for (int i = indicesAEliminar.size() - 1; i >= 0; i--) {
                        getZombie().remove((int) indicesAEliminar.get(i));                       
                    }
                    System.out.println("Se han muerto todos los corredores" + RESET);
                } 
            }
            break;         
		}	
	}
		
	// Se busca un arma y se la equipa el personaje
	public void buscar(Jugador jugador) {
	    Random rd = new Random();
	    int probabilidadEncontrarArma = rd.nextInt(10);
	    if (!getArma().isEmpty()) { // Verifica si quedan armas inicializadas para buscar
	    	if (probabilidadEncontrarArma < 5) {
	    		// Se elige aleatoriamente un índice de arma disponible
	            int randomIndice = rd.nextInt(getArma().size());
	            // Se obtiene el arma encontrada
	            Arma armaEncontrada = getArma().get(randomIndice);
	            
	            // Se agrega el arma actual del jugador al inventario
	            getArmaInicial().add(jugador.getArma());

	            // Se equipa el arma encontrada al jugador
	            jugador.setArma(armaEncontrada);
	            // Se elimina el arma encontrada de las armas disponibles
	            getArma().remove(randomIndice);

	            System.out.println("Has encontrado un arma: " + armaEncontrada.getNombre());
	    	} else {
	    		System.out.println("No has tenido suerte al buscar un arma.");
	    	}
	    } else {
	    	System.out.println("Ya no puedes encontrar más armas.");
	    }
	}
	//Sube la salud de los jugadores en 1 punto cada 3 niveles
	public void aumentarSalud() {
		if (getContNivel() >= 3) {
			for (Jugador jugador : getJugador()) {
				jugador.setSalud(jugador.getSalud()+1);
				jugador.setSaludMaxima(jugador.getSaludMaxima()+1);
			}
			setContNivel(0);
			System.out.println("\n" + CYAN + "Aumenta la salud de los jugadores en 1.");
		}
	}
	
	/*Cambia el arma del jugador por otra disponible*/
	public void cambiarArma(Jugador jugador) {
	    if (!getArmaInicial().isEmpty()) { // Verifica si hay armas disponibles para cambiar.
	        Scanner sc = new Scanner(System.in);

	        // Muestra las armas disponibles en un menú.
	        System.out.println("Armas disponibles:");
	        for (int i = 0; i < getArmaInicial().size(); i++) {
	            System.out.println((i + 1) + ". " + getArmaInicial().get(i).getNombre().toUpperCase() +
	                    " [DAÑO: " + getArmaInicial().get(i).getDaño() + " ALC: " + getArmaInicial().get(i).getAlcance() +
	                    " ACIER: " + getArmaInicial().get(i).getAcierto() + "]");
	        }

	        // El jugador elige el arma que quiere.
	        System.out.print("Elige el arma que quieres: ");
	        int opcion = sc.nextInt();

	        if (opcion >= 1 && opcion <= getArmaInicial().size()) {
	            // Guarda el arma actual del jugador en el conjunto de armas disponibles.
	            getArmaInicial().add(jugador.getArma());

	            // Asigna al jugador el arma elegida.
	            Arma armaElegida = getArmaInicial().get(opcion - 1);
	            jugador.setArma(armaElegida);

	            // Elimina el arma elegida del conjunto de armas disponibles.
	            getArmaInicial().remove(armaElegida);

	            // Muestra un mensaje indicando el cambio de arma.
	            System.out.println("Has cambiado el arma de " + jugador.getNombre() + " por " + armaElegida.getNombre());
	        } else {
	            System.out.println("Opción no válida");
	        }
	    } else {
	        System.out.println("No hay armas disponibles");
	    }
	}
	
	/*Se genera un índice con un zombie inicializado en el main*/
	private int randZombie(ArrayList<Zombie> zombies) {
		if (zombies.isEmpty()) {
	        return -1; 
	    }
		Random rd = new Random();
		return rd.nextInt(zombies.size());
	}
	// Se genera un índice para que los jugadores puedan ser atacados
	private int randJugador(ArrayList<Jugador> jugadores) {
		if (jugadores.isEmpty()) {
			return -1;
		}
        Random rd = new Random();
        return rd.nextInt(jugadores.size());
    }

	
	// Se inicializan los zombies después de pasar de nivel
	private static void initZombies(ArrayList<Zombie> zombies, int nivel) {
		 Random rd = new Random();
		   
		    for (int i = 0; i < nivel; i++) {
		        int tipoZombie = rd.nextInt(3);

		        switch (tipoZombie) {
		            case 0:
		                zombies.add(new Caminante("Caminante", 1, 1, true, 1, 1, "Caminante"));
		                break;
		            case 1:
		                zombies.add(new Gordo("Gordo", 2, 2, true, 1, 1, "Gordo"));
		                break;
		            case 2:
		                zombies.add(new Corredor("Corredor", 1, 1, true, 2, 1, "Corredor"));
		                break;
		        }
		    }
	}
}
