package main;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import armas.Arco;
import armas.Arma;
import armas.Espada;
import armas.Hacha;
import armas.Hechizo;
import humanoides.Caminante;
import humanoides.Corredor;
import humanoides.Gordo;
import humanoides.Jugador;
import humanoides.Zombie;

public class Zombicide {

	private static Scanner sc = new Scanner(System.in);
	private static ArrayList<Jugador> jugadores = new ArrayList<>();
	private static ArrayList<Arma> armas = new ArrayList<>();
	private static ArrayList<Zombie> zombies = new ArrayList<>();
	private static ArrayList<Arma> armasIniciales = new ArrayList<>();
	
	public static void main(String[] args) {
		int opcion; 
		initPersonajes();
		initObjetos();
		
		do { 
			menu();
			opcion = sc.nextInt();
			sc.nextLine();
			
			switch (opcion) {
				case 0:
					System.out.println("Saliendo de Zombicide.");
					System.exit(0);
					break;
				case 1:
					nuevaPartida();
					break;
				case 2:
					nuevoPersonaje();
					break;
				default:
					System.out.println("");
			}
		} while (opcion!=0);
		
	}

	//Menu principal
	public static void menu() {		
			System.out.println("BIENVENIDO A ZOMBICIDE");
			System.out.println("\n1- Nueva partida");
			System.out.println("2- Nuevo personaje");
			System.out.println("0- Salir");
			
	}
	//Se crea una nueva partida seleccionando a los personajes
	private static void nuevaPartida() {
		if (jugadores.size() == 3) { // Si son 3 jugadores empieza directamente la partida
	        initZombies(3);
	        Partida nuevaPartida = new Partida(jugadores, armas, 3, false, zombies, armasIniciales);
	        nuevaPartida.jugarPartida();
	    } else if (jugadores.size() > 3) { // Si son más de 3 personajes te pregunta con que personajes quieres jugar y empiezas la partida
	        System.out.println("Estos son tus personajes:");
	        for (int i = 0; i < jugadores.size(); i++) {
	            System.out.println((i + 1) + ". " + jugadores.get(i).getNombre());
	        }

	        int cantidadSeleccionada;
	        do {
	            System.out.print("Ingrese la cantidad de personajes (entre 3 y " + jugadores.size() + "): ");
	            cantidadSeleccionada = sc.nextInt();
	        } while (cantidadSeleccionada < 3 || cantidadSeleccionada > jugadores.size());

	        ArrayList<Jugador> jugadoresSeleccionados = new ArrayList<>();
	        ArrayList<Integer> indicesSeleccionados = new ArrayList<>(); 

	        for (int i = 0; i < cantidadSeleccionada; i++) {
	            int seleccion;
	            do {
	                System.out.print("Seleccione el número del personaje: ");
	                seleccion = sc.nextInt();
	                if (seleccion < 1 || seleccion > jugadores.size()) {
	                    System.out.println("Número de personaje no válido. Debe estar entre 1 y " + jugadores.size());
	                } else if (indicesSeleccionados.contains(seleccion)) { // Se verifica si el personaje ya ha sido seleccionado
	                    System.out.println("Este personaje ya ha sido seleccionado. Elija otro.");
	                }
	            } while (seleccion < 1 || seleccion > jugadores.size() || indicesSeleccionados.contains(seleccion));

	            indicesSeleccionados.add(seleccion); 

	            jugadoresSeleccionados.add(jugadores.get(seleccion - 1));
	        }
	        initZombies(cantidadSeleccionada);
	        Partida nuevaPartida = new Partida(jugadoresSeleccionados, armas, cantidadSeleccionada, false, zombies, armasIniciales);
	        System.out.println("Todos tus personajes han sido seleccionados");
	        nuevaPartida.jugarPartida();
	    }
	}
	/*Se crea un nuevo personaje y se guarda en un arraylist*/
	private static void nuevoPersonaje() {
		if (jugadores.size() == 10) {
			System.out.println("No puedes crear más personajes");
			System.out.println("");
		} else if (jugadores.size() < 10) {
		    System.out.print("Ingrese el nombre del nuevo personaje: ");
		    String nombre = sc.next();
		    Random rd = new Random();
		    int vida = rd.nextInt(4) + 5;
		    Jugador nuevoJugador = new Jugador(nombre, vida, vida, true, new Arma());
		    jugadores.add(nuevoJugador);
	
		    System.out.println("¡Nuevo personaje creado!");
		    System.out.println("");
		}
	}
	/*Inicializa los 3 personajes por defecto*/
	private static void initPersonajes() {
		jugadores.add(new Jugador("James", 7, 7, true, new Arma("Mandoble", 2, 1, 4)));
		jugadores.add(new Jugador("Marie", 5, 5, true, new Arma()));
		jugadores.add(new Jugador("Jaci", 5, 5, true, new Arma()));
	}
	//Se guardan en un arraylist las armas creadas
	private static void initObjetos() {
		armas.add(new Arco("Arco Largo", 1, 2, 3));
	    armas.add(new Hacha("Hacha doble", 2, 1, 3));
	    armas.add(new Hechizo("Bola de fuego", 3, 1, 4));
	    armas.add(new Espada("Espada corta", 1, 1, 4));
	    armas.add(new Espada("Espada larga", 3, 2, 3));
	    armas.add(new Arco("Ballesta", 1, 2, 5));
	    armas.add(new Espada("Espada de Fuego", 2, 2, 4));
	    armas.add(new Arma("Guadaña", 2, 1, 5));
	    armas.add(new Arma("Lanza", 1, 1, 7));
	    armas.add(new Hechizo("Viento helado", 1, 2, 5));
	    armas.add(new Arma("Martillo", 3, 1, 2));
	    armas.add(new Hechizo("Descarga eléctrica", 1, 4, 5));
	    armas.add(new Espada("Espada mágica", 2, 2, 5));
	    armas.add(new Arma("Gran lanza", 3, 1, 6));
	    armas.add(new Hacha("Hacha de Guerra", 2, 2, 4));
	    armas.add(new Hacha("Arco corto", 1, 1, 4));
	    
	    Random rand = new Random();
	    for (int i = 0; i < 3; i++) {
	        int randIndex = rand.nextInt(armas.size());
	        armasIniciales.add(armas.get(randIndex));
	        armas.remove(randIndex);
	    }
	}
	
	
	// Inicializa los zombies de la primera ronda
	public static void initZombies(int nivel) {	
		Random rd = new Random();
		zombies.clear();
		for (int i = 0; i < nivel; i++) {
	        int tipoZombie = rd.nextInt(3);
	        switch (tipoZombie) {
	            case 0:
	                zombies.add(new Caminante("Caminante", 1, 1, true, 1, 1, "Caminante"));
	                break;
	            case 1:
	                zombies.add(new Gordo("Gordo", 2, 2, true, 1, 1, "Gordo"));
	                break;
	            case 2:
	                zombies.add(new Corredor("Corredor", 1, 1, true, 2, 1, "Corredor"));
	                break;
	        }
	    }
	}
}
